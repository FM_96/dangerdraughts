package dangerAi;



import xyz.dangerdraughts.interfaces.AiHelper;
import xyz.dangerdraughts.interfaces.Move;
import xyz.dangerdraughts.interfaces.MoveRequest;
import xyz.dangerdraughts.interfaces.Player;
import xyz.dangerdraughts.interfaces.Settings;

public class MyFirstAi implements Player {
	
	private Settings settings;
	private AiHelper helper;
	private String yourColor;
	private String opponentColor;
	
	@Override
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
	
	@Override
	public void setColors(String yourColor, String opponentColor) {
		this.yourColor = yourColor;
		this.opponentColor = opponentColor;
	}
	
	@Override
	public void setAiHelper(AiHelper helper) {
		this.helper = helper;
	}

	@Override
	public Move makeMove(MoveRequest moveReq) {
		Move chosen = moveReq.getLegalMoves().get((int)((Math.random()*10000)%moveReq.getLegalMoves().size()));
		System.out.println(chosen.toStringPdn());
		return chosen;
	}

}
